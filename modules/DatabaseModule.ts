import { Module } from './module';
import { ConfigType } from '../config.type';
import * as Knex from 'knex';

export class DatabaseModule implements Module {
  public name: string = 'DatabaseModule';
  private knexInstance!: Knex;

  public async init(config: ConfigType): Promise<void> {
    const dbPath = config.dbPath || './db.sqlite';
    this.knexInstance = require('knex')(
      {
        client: 'sqlite3',
        connection: {
          filename: dbPath
        },
        useNullAsDefault: true
      }
    );
  }

  public getDb() {
    return this.knexInstance;
  }
}

