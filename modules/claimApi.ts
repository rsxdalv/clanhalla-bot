import request from 'request-promise-native';
import { Module } from './module';
import { ConfigType } from '../config.type';

export class ClaimApiModule implements Module {
  private static readonly CLAIM_API_URL = 'http://claim.vorpallongspear.com';
  public name: string = 'ClaimApiModule';

  private apiKey!: string;

  public async init(config: ConfigType): Promise<void> {
    this.apiKey = config.claimApi;
  }

  public async getUser(id: string){
    return await this.doRequest(`claim/${encodeURIComponent(id)}`);
  }

  public async saveUser(discordId: string, bhid: number) {
    return await this.doRequest(`claim`, 'post', {discordId, bhid});
  }

  private async doRequest(path: string, method = 'get', body = {}): Promise<GerardClaim> {
    const options = {
      method,
      uri: `${ClaimApiModule.CLAIM_API_URL}/${path}`,
      qs: {
        API_KEY: this.apiKey,
      },
      json: true,
      form: body,
    };
    return await request(options);
  }
}

export interface GerardClaim {
  discordId: string;
  bhid: number;
  challonge: string;
  twitch: null | string;
}
