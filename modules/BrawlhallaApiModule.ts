import { Module } from './module';
import { ConfigType } from '../config.type';
import {BrawlhallaApi} from "brawlhalla-api-ts";
import {BrawlhallaRequest} from "brawlhalla-api-ts/utils/BrawlhallaRequest";

export class BrawlhallaApiModule extends BrawlhallaApi implements Module {
  public name = 'BrawlhallaApiModule';

  constructor(){
      // Key will be populated in the init method
      super('');
  }

  public async init(config: ConfigType): Promise<void> {
      this.brawlhallaApiKey = config.bhApi;
      this['_brawlhallaApiKey'] = config.bhApi;
      // @ts-ignore
      this['bhRequest'] = new BrawlhallaRequest(config.bhApi, BrawlhallaApi['apiUrl']);
  }
}
