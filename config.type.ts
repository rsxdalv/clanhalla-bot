export type ConfigType = {
  discordToken: string,
  bhApi: string,
  claimApi: string,

  dbPath?: string,
  defaultPrefix?: string,
  dbUserTableName?: string,
  dbGuildTableName?: string,
}
