const claim = {
    name: 'claim',
    description: 'Claim your Brawlhalla Profile',
    usage: '`*claim [1234 - Your brawhalla ID]`',
    arguments: [/(\d){1,9}/],
    execute: async ({message, user}, bhid) => {
        await user.createOrUpdate({
            bhId: bhid,
            discordId: message.author.id,
            username: message.author.name,
        });
        return 'Succesfully saved!';
    },
};

module.exports = claim;