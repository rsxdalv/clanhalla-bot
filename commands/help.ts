import { Command, CommandArgument } from './command';
import { ContextType } from '../bot';
import { Message } from 'discord.js';
import { PaginatedRichEmbed } from '../utils/PaginatedRichEmbed';

export class HelpCommand implements Command {
  public readonly name = 'Help';
  public readonly alias = [ 'help' ];
  public description = 'Help about the available commands!';
  public arguments: CommandArgument[] = [ { type: 'command', required: false } ];
  public usage = 'help [Optional: command]';

  private helpEmbed!: PaginatedRichEmbed;

  public init(){}

  public initHelp(commands: Command[]){
    const embed = new PaginatedRichEmbed();
    embed.setTitle('Help!');
    embed.addField('Description', this.description);

    commands.forEach(c => {
      embed.addField(c.name, c.description);
    });
    this.helpEmbed = embed;
  }

  public execute(ctx: ContextType, message: Message, command?: Command): PaginatedRichEmbed {
    return this.helpEmbed;
  }
}

export const HelpInstance  = new HelpCommand();

