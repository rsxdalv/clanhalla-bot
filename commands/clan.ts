import { Command, CommandArgument } from './command';
import { ContextType, ModuleType } from '../bot';
import { Message } from 'discord.js';
import { PaginatedRichEmbed } from '../utils/PaginatedRichEmbed';
import { IClan, IPlayerStats } from 'brawlhalla-api-ts-interfaces';
import { BrawlhallaApiModule } from '../modules';

export class ClanCommand implements Command {
  public name: string = 'Clan';
  public description: string = 'View clan information';
  public alias: string[] = [ 'clan' ];
  public arguments: CommandArgument[] = [{
    type: 'brawlhalla_profile',
    required: true,
    default: 'CURRENT_USER'
  }];
  public usage: string = 'clan @Mention';

  private BrawlhallaApiModule!: BrawlhallaApiModule;

  public init({BrawlhallaApiModule}: ModuleType){
    this.BrawlhallaApiModule = BrawlhallaApiModule;
  }

  public async execute(
    context: ContextType,
    message: Message,
    bhProfile: IPlayerStats): Promise<PaginatedRichEmbed | string> {
    if(!bhProfile.clan){
      return 'The mentioned user does not have a clan!';
    }
    const clanNumber = bhProfile.clan.id;
    const clan = await this.BrawlhallaApiModule.getClanInfo(clanNumber);
    return this.buildClanEmbed(clan, clanNumber);
  }

  private buildClanEmbed(clan: IClan, id: number): PaginatedRichEmbed {
    const embed = new PaginatedRichEmbed()
      .setTitle(clan.name)
      .setColor('GOLD')
      .setURL(`https://brawldb.com/clan/info/${id}`);
    const creationDate = new Date(clan.creationDate * 1000);
    embed.addField('Clan information',
      `Creation: ${creationDate.toLocaleDateString('en-US')}\nExperience: ${clan.xp}`,
      false);
    embed.addField('Members', 'List of members below', false);

    clan.members.forEach(member => {
      const joinDate = new Date(member.joinDate * 1000);
      embed.addField(member.name, `[BrawlDB](https://brawldb.com/player/stats/${member.brawlhallaID})\n` +
        `${member.rank} - ${member.contributedExperience}xp - Joined ${joinDate.toLocaleDateString('en-US')}\n`);
    });
    return embed;
  }
}

export const ClanInstance = new ClanCommand();
