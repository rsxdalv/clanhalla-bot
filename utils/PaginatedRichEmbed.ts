import {RichEmbed} from 'discord.js';
import {ReactiveRichEmbed} from './ReactiveEmbed';

export class PaginatedRichEmbed extends ReactiveRichEmbed {
  public itemsPerPage: number;

  public fields: { name: string; value: string; inline?: boolean; isPaginated?: boolean }[];

  private currentPage: number;
  private previousPageIcon: string = '◀';
  private nextPageIcon: string = '▶';
  private firstPageIcon: string = '⏮';
  private lastPageIcon: string = '⏭';

  public get pageCount(): number {
    const paginatedItemsCount = this.fields.filter(f => f.isPaginated === undefined || f.isPaginated === true).length;
    let pageCount = ~~(paginatedItemsCount / this.itemsPerPage);
    if (paginatedItemsCount % this.itemsPerPage !== 0) {
      pageCount++;
    }
    return pageCount;
  }

  constructor() {
    super();
    this.fields = [];
    this.addReactionListener(this.firstPageIcon, async () => this.goToFirstPage());
    this.addReactionListener(this.previousPageIcon, async () => this.goToPreviousPage());
    this.addReactionListener(this.nextPageIcon, async () => this.goToNextPage());
    this.addReactionListener(this.lastPageIcon, async () => this.goToLastPage());
    this.itemsPerPage = 5;
    this.currentPage = 0;
  }

  public build(): RichEmbed {
    const generatedEmbed = new RichEmbed();
    if (this.author) {
      generatedEmbed.setAuthor(this.author);
    }
    if (this.color) {
      generatedEmbed.setColor(this.color);
    }
    if (this.description) {
      generatedEmbed.setDescription(this.description);
    }
    let footerMessage = (this.footer || '') + `\nPage ${this.currentPage! + 1} / ${this.pageCount}`;
    generatedEmbed.setFooter(footerMessage);
    if (this.thumbnail) {
      generatedEmbed.setThumbnail(this.thumbnail);
    }
    if (this.title) {
      generatedEmbed.setTitle(this.title);
    }
    if (this.url) {
      generatedEmbed.setURL(this.url);
    }

    // While iterating through the fields, only count the required fields in the pagination
    let currentPaginatedElementCount = 0;
    let currentPaginatedElementAdded = 0;
    const paginatedElementStart = this.currentPage * this.itemsPerPage;
    for (let i = 0; i < this.fields.length; i++) {
      const field = this.fields[i];
      if (field.isPaginated === undefined || field.isPaginated === true) {
        currentPaginatedElementCount++;
        if (currentPaginatedElementCount > paginatedElementStart) {
          currentPaginatedElementAdded++;
        }
      }
      // Add all non-paginated fields, and enough of the paginated ones to cover one page
      if (field.isPaginated === false ||
        (currentPaginatedElementCount > paginatedElementStart && currentPaginatedElementAdded <= this.itemsPerPage)) {
        if (field.name === 'BLANK') {
          generatedEmbed.addBlankField(field.inline);
        } else {
          generatedEmbed.addField(field.name, field.value, field.inline);
        }
      }
    }

    return generatedEmbed;
  }

  public async goToPreviousPage(): Promise<void> {
    if (this.hasPreviousPage) {
      this.currentPage!--;
    }
  }

  public goToFirstPage(): void {
    this.currentPage = 0;
  }

  public goToLastPage(): void {
    this.currentPage = this.pageCount - 1;
  }

  public async goToNextPage(): Promise<void> {
    if (this.hasNextPage) {
      this.currentPage!++;
    }
  }

  private get hasPreviousPage(): boolean {
    return this.currentPage! > 0;
  }

  private get hasNextPage(): boolean {
    return this.currentPage! < this.pageCount - 1;
  }
}
