const fs = require('fs');
const BhApi = require('brawlhalla-api-ts');

const db = require('./modules/DatabaseModule');
const server = require('./modules/ServerModule');
const user = require('./modules/user');
const claimApi = require('./modules/claimApi');

const ping = require('./commands/ping');
const prefix = require('./commands/prefix');
const claim = require('./commands/claim');
const clan = require('./commands/clan');

const config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

const discordUsersTableName = config.dbUserTable || 'discordUser';

const Discord = require('discord.js');

let knex;
let bhApi;
const allCommands = [ping, prefix, claim, clan];

// Generating help command
const helpCommand = new Discord.RichEmbed()
    .setTitle('Clanhalla bot help')
    .setDescription('List of commands and their usage')
    .setColor('PURPLE');
allCommands.forEach(c => {
    // Will only work for up to 25 fields!
    helpCommand.addField(c.name, `${c.description}\nUsage: ${c.usage}`);
});

allCommands.push({
    name: 'help',
    execute: _ => helpCommand,
    description: 'Help command',
    usage: '*help',
});

const commands = new Map(allCommands.map(c => [c.name, c]));

async function init() {
    knex = await db.getDb(config);
    bhApi = new BhApi.BrawlhallaApi(config.bhApi);
    await server.init(knex, config);
    await user.init(knex, config);
    claimApi.init(config);
}

async function botLoop() {
    const client = new Discord.Client();
    await client.login(config.discordToken);
    client.on('message', async (message) => {
        const currentServer = await server.getServerConfig(message.guild);
        if (!message.content.startsWith(currentServer.prefix)) {
            return;
        }
        const [commandName, ...params] = message.content.substring(currentServer.prefix.length).split(' ');

        if (!commands.has(commandName)) {
            // Invalid command
            return;
        }
        const command = commands.get(commandName);
        const argumentTypes = (command.arguments || []);
        const areArgumentsValid = params.length >= argumentTypes.length && argumentTypes.every((type, i) => {
            return isArgumentTypeValid(type, params[i]);
        });
        if (!areArgumentsValid) {
            message.channel.send('Invalid usage!\nTry the following:');
            message.channel.send(command.usage);
            return;
        }

        const parsedArguments = parseArguments(params, argumentTypes);

        try {
            const result = await command.execute(
                {message, server: currentServer, user, bhApi, claimApi},
                ...parsedArguments
            );
            message.channel.send(result);
        } catch (e) {
            console.error(e);
            message.channel.send(e.toString());
        }

    });
}

const discordIdRegex = /<@[\d]{18}>/;

function isArgumentTypeValid(type, value) {
    if (type === 'string') {
        return typeof value === 'string';
    } else if (type === 'number') {
        return !isNaN(+value)
    } else if (type === 'mention') {
        return value.match(discordIdRegex);
    } else if (type instanceof RegExp) {
        return value.match(type);
    } else {
        return true;
    }
}

function parseArguments(values, types) {
    return types.map((type, index) => {
        const value = values[index];
        if (type === 'string') {
            return value;
        } else if (type === 'number') {
            return +value;
        } else if (type === 'mention') {
            const id = value.substr(2,18);
            return id;
        } else if (type instanceof RegExp) {
            return type.exec(value);
        } else {
            return value;
        }
    });
}

(async () => {
    await init();
    botLoop();
})();
